package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import data.getConnection;
import model.Report;
import model.Visitor;
import model.Weibo;

/**
 * @Author 作者
 * @Description 说明：修改
 * @Date 时间：2020-12-10
 */
public class Updata {
	private Connection connection;
	private PreparedStatement preSQL;

	/**
	 * @Description 说明：构造方法
	 */
	public Updata() {
		connection = getConnection.GetConnection("desktopweibo", "root", "");
	}

	/**
	 * @Description 说明：修改访客
	 */
	public Visitor updata(Visitor visitor) {
		int ok = 0;
		// SQL语句
		String sqlString = "update visitor set visitor_name=?,visitor_sex=?,visitor_birthday=?,visitor_state=?,visitor_weibo_num=?,visitor_attention_num=?,visitor_fans_num=? where id=?";
		try {
			// 发送sql语句
			preSQL = connection.prepareStatement(sqlString);
			// 设置要传入的参数
			preSQL.setString(1, visitor.getVisitor_name());
			preSQL.setString(2, visitor.getVisitor_sex());
			preSQL.setString(3, visitor.getVisitor_birthday());
			preSQL.setString(4, visitor.getVisitor_state());
			preSQL.setInt(5, visitor.getVisitor_weibo_num());
			preSQL.setInt(6, visitor.getVisitor_attention_num());
			preSQL.setInt(7, visitor.getVisitor_fans_num());
			preSQL.setString(8, visitor.getId());
			// 执行sql语句
			ok = preSQL.executeUpdate();
			connection.close();
		} catch (SQLException e) {
			System.out.println("没有修改访客对象");
			System.out.println(e);
		}
		if (ok != 0) {
			System.out.println("已经成功修改访客对象");
		}
		return visitor;
	}

	/**
	 * @Description 说明：修改微博
	 */
	public Weibo updata(Weibo weibo) {
		int ok = 0;
		// SQL语句
		String sqlString = "update weibo set weibo_like_num=?,weibo_true_false=?,weibo_state=? where weibo_id = ?";
		try {
			// 发送sql语句
			preSQL = connection.prepareStatement(sqlString);
			// 设置要传入的参数
			preSQL.setInt(1, weibo.getWeibo_like_num());
			preSQL.setString(2, weibo.getWeibo_true_false());
			preSQL.setString(3, weibo.getWeibo_state());
			preSQL.setString(4, weibo.getWeibo_id());
			// 执行sql语句
			ok = preSQL.executeUpdate();
			connection.close();
		} catch (SQLException e) {
			System.out.println("没有修改微博对象");
			System.out.println(e);
		}
		if (ok != 0) {
			System.out.println("已经成功修改微博对象");
		}
		return weibo;
	}

	/**
	 * @Description 说明：修改举报
	 */
	public Report updata(Report report) {
		int ok = 0;
		// SQL语句
		String sqlString = "update report set operator_id=?,report_state=?,report_num=?,report_content=? where report_id=?";
		try {
			// 发送sql语句
			preSQL = connection.prepareStatement(sqlString);
			// 设置要传入的参数
			preSQL.setString(1, report.getOperator_id());
			preSQL.setString(2, report.getReport_state());
			preSQL.setInt(3, report.getReport_num());
			preSQL.setString(4, report.getReport_content());
			preSQL.setString(5, report.getReport_id());
			// 执行sql语句
			ok = preSQL.executeUpdate();
			connection.close();
		} catch (SQLException e) {
			System.out.println("没有修改举报对象");
			System.out.println(e);
		}
		if (ok != 0) {
			System.out.println("已经成功修改举报对象");
		}
		return report;
	}
}
