package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import data.Constant;
import data.GlobalVar;
import data.getConnection;
import model.Operator;
import model.Personal;
import model.Visitor;

/**
 * @Author 作者
 * @Description 说明：登录注册
 * @Date 时间：2020-12-2
 */
public class LoginRegister {

	private Connection connection;
	private PreparedStatement preSQL;
	private ResultSet resultSet;

	/**
	 * @Description 说明：构造方法
	 */
	public LoginRegister() {
		connection = getConnection.GetConnection("desktopweibo", "root", "");
	}

	/**
	 * @Description 说明：登录——查找个人、判断权限进行查找
	 */
	public Personal loginPersonal(Personal login) {
		String id = login.getId();
		String password = login.getPassword();
		// SQL语句
		String sqlString = "select id,password,power from personal where " + "id = ? and password = ?";
		try {
			// 发送sql语句
			preSQL = connection.prepareStatement(sqlString);
			// 设置要传入的参数
			preSQL.setString(1, id);
			preSQL.setString(2, password);
			// 执行sql语句
			resultSet = preSQL.executeQuery();
			// 检查是否已经注册的用户
			if (resultSet.next()) {
				login.setId(resultSet.getString("id"));
				login.setPassword(resultSet.getString("password"));
				login.setPower(resultSet.getString("power"));
				login.setLogin_success(Constant.YES_STRING);
				GlobalVar.login_personal = login;// 当前登录个人
				// 获取个人权限并判断是否存在该用户
				switch (login.getPower()) {
				case "访客":
					/* 访客 */
					Visitor visitor = new Visitor(id);
					new Select().select(visitor);// 查找访客，判断访客是否登录
					if (visitor.getVisitor_yes_no().equals(Constant.YES_STRING) == true) {
						GlobalVar.login_visitor = visitor;// 当前登录访客

						String s1[] = new Select().select_Attention_id(GlobalVar.login_visitor.getId());
						for (int i = 0; i < s1.length; i++) {
							Visitor[] visitors1 = new Select().select_Visitor_id(s1[i]);
							for (int j = 0; j < visitors1.length; j++) {
								GlobalVar.weibo_num2 = GlobalVar.weibo_num2+new ResultSetNum().resultSetNumWeibo(visitors1[j].getId());
							}
						}
						GlobalVar.weibo_num1 = GlobalVar.login_visitor.getVisitor_weibo_num();
						GlobalVar.attention_num = GlobalVar.login_visitor.getVisitor_attention_num();
						GlobalVar.fans_num = GlobalVar.login_visitor.getVisitor_fans_num();

						System.out.println("-----登录访客对象的部分信息");
						System.out.println("访客ID：" + GlobalVar.login_visitor.getId());
						System.out.println("访客昵称：" + GlobalVar.login_visitor.getVisitor_name());
						System.out.println("可见微博总个数：" + GlobalVar.weibo_num2);
						System.out.println("微博个数：" + GlobalVar.weibo_num1);
						System.out.println("关注个数：" + GlobalVar.attention_num);
						System.out.println("粉丝个数：" + GlobalVar.fans_num);

					} else {
						System.out.println("不可查看");
					}
					break;
				case "操作员":
					/* 操作员 */
					Operator operator = new Operator(id);
					new Select().select(operator);
					if (operator.getOperator_yes_no().equals(Constant.YES_STRING) == true) {
						System.out.println("可查看");
						GlobalVar.login_operator = operator;// 当前登录操作员
						System.out.println("-----登录操作员对象的信息");
						System.out.println("操作员账号：" + GlobalVar.login_operator.getId());
						System.out.println("访客待审核数：" + GlobalVar.login_operator.getOperator_visitor_wait());
						System.out.println("访客已审核数：" + GlobalVar.login_operator.getOperator_visitor_complete());
						System.out.println("微博待审核数：" + GlobalVar.login_operator.getOperator_weibo_wait());
						System.out.println("微博已审核数：" + GlobalVar.login_operator.getOperator_weibo_complete());
					} else {
						System.out.println("不可查看");
					}
					break;
				case "管理员":
					System.out.println("查找管理员");
					break;
				default:
					System.out.println("完成");
				}
			}
			connection.close();
		} catch (SQLException e) {
		}
		return login;
	}
}
