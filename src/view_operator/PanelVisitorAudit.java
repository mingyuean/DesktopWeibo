package view_operator;

import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import data.GlobalVar;
import tools.AfAnyWhere;
import tools.BackgroundPanel;

/**
 * @Author 作者
 * @Description 说明：访客审核
 * @Date 时间：2020-12-2
 */
@SuppressWarnings("serial")
public class PanelVisitorAudit extends JTabbedPane {
	// 选项卡面板
	private BackgroundPanel p1 = new BackgroundPanel(new ImageIcon("images\\背景_2.jpg").getImage());
	private BackgroundPanel p2 = new BackgroundPanel(new ImageIcon("images\\背景_2.jpg").getImage());

	public PanelVisitorAudit() {
		super(JTabbedPane.TOP);// 设置标签显示位置
		p1.setLayout(new AfAnyWhere());
		p2.setLayout(new AfAnyWhere());
	}

	/**
	 * @Description 说明：访客审核面板初始化
	 */
	public void init(int w, int h) {

		JScrollPane scrollPane1 = new JScrollPane(p1);
		this.add("待审核" + GlobalVar.login_operator.getOperator_visitor_wait(), scrollPane1);

		JScrollPane scrollPane2 = new JScrollPane(p2);
		this.add("已审核" + GlobalVar.login_operator.getOperator_visitor_complete(), scrollPane2);
	}
}
