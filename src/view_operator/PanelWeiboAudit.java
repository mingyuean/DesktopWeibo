package view_operator;

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import control.Select;
import data.Constant;
import data.GlobalVar;
import model.Weibo;
import tools.AfAnyWhere;
import tools.AfMargin;
import tools.BackgroundPanel;
import view_visitor.PanelTXT;

/**
 * @Author 作者
 * @Description 说明：微博审核
 * @Date 时间：2020-12-2
 */
@SuppressWarnings("serial")
public class PanelWeiboAudit extends JTabbedPane {
	// 选项卡面板
	private BackgroundPanel p1 = new BackgroundPanel(new ImageIcon("images\\背景_2.jpg").getImage());
	private BackgroundPanel p2 = new BackgroundPanel(new ImageIcon("images\\背景_2.jpg").getImage());

	public PanelWeiboAudit() {
		super(JTabbedPane.TOP);// 设置标签显示位置
		p1.setLayout(new AfAnyWhere());
		p2.setLayout(new AfAnyWhere());
	}

	/**
	 * @Description 说明：微博审核面板初始化
	 */
	public void init(int w, int h) {
		// 待审核
		p1.setPreferredSize(new Dimension(300, Constant.NUM * GlobalVar.weibo_num2));
		// 根据当前操作员的id获取所有需审核的id数组
		// 根据id数组获取微博数组(查找举报对象——通过Operator_id)
		String s[] = new Select().select_Operator_id(GlobalVar.login_operator.getId());
		int sum = 0;
		for (int i = 0; i < s.length; i++) {
			Weibo[] weibos = new Select().select_Writer_id(s[i]);
			for (int nm = 0; nm < weibos.length; nm++) {
				p1.add(new PanelTXT(weibos[nm]), new AfMargin(Constant.NUM * sum, 0, -1, 0));
				sum++;
			}
		}
		JScrollPane scrollPane1 = new JScrollPane(p1);
		this.add("待审核" + GlobalVar.login_operator.getOperator_weibo_wait(), scrollPane1);

		// 已审核
		p2.setPreferredSize(new Dimension(300, Constant.NUM * GlobalVar.weibo_num1));
		// 通过当前访客id获取微博数组
//		Weibo[] weibos = new Select().select_Writer_id(GlobalVar.login_visitor.getId());
//		for (int i = 0; i < weibos.length; i++) {
//			p2.add(new PanelTXT(weibos[i]), new AfMargin(Constant.NUM * i, 0, -1, 0));
//		}
		JScrollPane scrollPane2 = new JScrollPane(p2);
		this.add("已审核" + GlobalVar.login_operator.getOperator_weibo_complete(), scrollPane2);
	}
}
