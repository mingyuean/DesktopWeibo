package view_operator;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import control.Select;
import data.Constant;
import model.Visitor;
import model.Weibo;
import tools.AfAnyWhere;
import tools.AfMargin;
import tools.BackgroundPanel;

/**
 * @Author 作者
 * @Description 说明：操作员窗口
 * @Date 时间：2020-12-2
 */
@SuppressWarnings("serial")
public class Interface_MainFrame extends JFrame {
	// 工具栏
	private JToolBar jToolBar = new JToolBar();
	private JButton Jbutton1, Jbutton2, Jbutton3;
	private JTextField idTextField;

	// 按钮（操作员信息、访客审核、微博审核、查看访客）
	private JButton button1, button2, button3, button4;
	// 创建中间容器[设置布局]
	private BackgroundPanel panel1, panel2;
	// 分割面板VERTICAL\HORIZONTAL
	private JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true);

	/**
	 * 构造方法
	 */
	public Interface_MainFrame() {
		this.setVisible(false);
		// 设置窗口标题
		this.setTitle("操作员界面");
		// 设置窗口大小
		this.setSize(600, 500);
		// 居中显示
		this.setLocationRelativeTo(null);

		// 设置图标
		this.setIconImage(new ImageIcon("images\\图标_微博.png").getImage());

		// 设置点击关闭窗口后做出的处理
		// JFrame.DO_NOTHING_ON_CLOSE 什么也不做
		// JFrame.HIDE_ON_CLOSE 隐藏当前窗口
		// JFrame.DISPOSE_ON_CLOSE 隐藏当前窗口，并释放窗体占有的其他资源
		// JFrrame.EXIT_ON_CLOSE 结束窗口所在应用程序
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		/* 面板设置 */
		this.panelSettings();
		/* 事件监听 */
		this.addListener();

		// 添加工具栏
		this.add(jToolBar, BorderLayout.NORTH);
		// 将分割面板添加到窗口
		this.add(splitPane);
		// 设置窗口是否可缩放
		this.setResizable(false);
		// 设置窗口是否可见
		this.setVisible(true);

	}

	/**
	 * @Description 说明：面板设置
	 */
	private void panelSettings() {

		/* 面板1设置 */
		panel1 = new BackgroundPanel(new ImageIcon("images\\背景_1.jpg").getImage());
		panel1.setLayout(null);
		// 按钮
		button1 = new JButton("操作员信息");
		button1.setFont(Constant.FONT2);
		button1.setBounds(0, 45, 100, 40);
		button2 = new JButton("访客审核");
		button2.setFont(Constant.FONT2);
		button2.setBounds(0, 115, 100, 40);
		button3 = new JButton("微博审核");
		button3.setFont(Constant.FONT2);
		button3.setBounds(0, 185, 100, 40);
		button4 = new JButton("查看访客");
		button4.setFont(Constant.FONT2);
		button4.setBounds(0, 255, 100, 40);
		// 添加组件
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		panel1.add(button4);

		// 设置工具栏
		setJToolBar();
		// 面板2设置
		setupPanel();

		/* 分割面板设置 */
		splitPane.setLeftComponent(panel1);
		splitPane.setRightComponent(panel2);
		// 设置分割线大小
		splitPane.setDividerSize(0);
		// 设置分割线位置
		splitPane.setDividerLocation(100);
		// 设置分割线拖动
		splitPane.setEnabled(false);
		// 在分隔符上提供UI小部件以快速扩展/折叠分隔符
		splitPane.setOneTouchExpandable(false);
	}

	/**
	 * 面板panel2初始界面
	 */
	private void setupPanel() {
		// TODO 自动生成的方法存根
		panel2 = new BackgroundPanel(new ImageIcon("images\\背景_2.jpg").getImage());
		JLabel Title = new JLabel("欢迎来到操作员界面！");
		Title.setFont(Constant.FONT1);
		panel2.add(Title);
	}

	/**
	 * @Description 说明：设置工具栏
	 */
	private void setJToolBar() {
		// 设置工具栏不可浮动
		jToolBar.setFloatable(false);
		// 设置工具栏边框导角方式
		jToolBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
		jToolBar.setBackground(Color.yellow);

		idTextField = new JTextField("请输入需要查找的编号", 25);
		Jbutton1 = new JButton("搜索访客");
		Jbutton2 = new JButton("搜索微博");
		Jbutton3 = new JButton("刷新");

		jToolBar.add(idTextField);
		jToolBar.add(Jbutton1);
		jToolBar.add(Jbutton2);
		jToolBar.add(Jbutton3);
	}

	/**
	 * @Description 说明：事件监听
	 */
	private void addListener() {
		// TODO 自动生成的方法存根
		button1.addActionListener((e) -> setup_PanelInformation(panel2, new PanelInformation()));
		button2.addActionListener((e) -> setup_PanelVisitorAudit(panel2, new PanelVisitorAudit()));
		button3.addActionListener((e) -> setup_PanelWeiboAudit(panel2, new PanelWeiboAudit()));
		button4.addActionListener((e) -> setup_PanelShow(panel2, new PanelShow()));
		/* 搜索访客 */
		Jbutton1.addActionListener((e) -> {
			Visitor visitor = new Visitor(idTextField.getText());
			new Select().select(visitor);
			if (visitor.getVisitor_yes_no().equals(Constant.YES_STRING) == true) {
				FindVisitor dialog1 = new FindVisitor(visitor, this, true);
				dialog1.setVisible(true);
			} else {
				System.out.println("不可查看");
				JOptionPane.showMessageDialog(null, "不存在", "警告", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		/* 搜索微博 */
		Jbutton2.addActionListener((e) -> {
			Weibo weibo = new Weibo(idTextField.getText());
			new Select().select(weibo);
			if (weibo.getWeibo_state().equals(Constant.WEIBO_STATE[0]) == true || weibo.getWeibo_state().equals(Constant.WEIBO_STATE[1]) == true) {
				FindWeibo dialog2 = new FindWeibo(weibo, this, true);
				dialog2.setVisible(true);
			} else {
				System.out.println("不可查看");
				JOptionPane.showMessageDialog(null, "不存在", "警告", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		/* 刷新 */
		Jbutton3.addActionListener((e) -> {
			new Interface_MainFrame();
		});
	}

	private Object setup_PanelShow(BackgroundPanel panel, PanelShow panelShow) {
		// TODO 自动生成的方法存根
		panel.removeAll();// 移除面板中的所有组件
		panel.add(panelShow);// 添加要切换的面板
		panel.repaint();// 刷新页面，重绘面板
		panel.validate();// 使重绘的面板确认生效

		// 初始化
		panelShow.init(panel.getWidth(), panel.getHeight());

		// 更换分割面板
		splitPane.setLeftComponent(panel1);
		splitPane.setRightComponent(panelShow);
		// 设置分割线大小
		splitPane.setDividerSize(0);
		// 设置分割线位置
		splitPane.setDividerLocation(100);
		// 设置分割线拖动
		splitPane.setEnabled(false);
		// 在分隔符上提供UI小部件以快速扩展/折叠分隔符
		splitPane.setOneTouchExpandable(false);
		return null;
	}

	private Object setup_PanelWeiboAudit(BackgroundPanel panel, PanelWeiboAudit panelWeiboAudit) {
		// TODO 自动生成的方法存根
		panel.removeAll();// 移除面板中的所有组件
		panel.add(panelWeiboAudit);// 添加要切换的面板
		panel.repaint();// 刷新页面，重绘面板
		panel.validate();// 使重绘的面板确认生效

		// 初始化
		panelWeiboAudit.init(panel.getWidth(), panel.getHeight());

		// 更换分割面板
		splitPane.setLeftComponent(panel1);
		splitPane.setRightComponent(panelWeiboAudit);
		// 设置分割线大小
		splitPane.setDividerSize(0);
		// 设置分割线位置
		splitPane.setDividerLocation(100);
		// 设置分割线拖动
		splitPane.setEnabled(false);
		// 在分隔符上提供UI小部件以快速扩展/折叠分隔符
		splitPane.setOneTouchExpandable(false);
		return null;
	}

	private Object setup_PanelVisitorAudit(BackgroundPanel panel, PanelVisitorAudit panelVisitorAudit) {
		// TODO 自动生成的方法存根
		panel.removeAll();// 移除面板中的所有组件
		panel.add(panelVisitorAudit);// 添加要切换的面板
		panel.repaint();// 刷新页面，重绘面板
		panel.validate();// 使重绘的面板确认生效

		// 初始化
		panelVisitorAudit.init(panel.getWidth(), panel.getHeight());

		// 更换分割面板
		splitPane.setLeftComponent(panel1);
		splitPane.setRightComponent(panelVisitorAudit);
		// 设置分割线大小
		splitPane.setDividerSize(0);
		// 设置分割线位置
		splitPane.setDividerLocation(100);
		// 设置分割线拖动
		splitPane.setEnabled(false);
		// 在分隔符上提供UI小部件以快速扩展/折叠分隔符
		splitPane.setOneTouchExpandable(false);
		return null;
	}

	private Object setup_PanelInformation(BackgroundPanel panel, PanelInformation panelInformation) {
		// TODO 自动生成的方法存根
		panel.removeAll();// 移除面板中的所有组件
		panel.add(panelInformation);// 添加要切换的面板
		panel.repaint();// 刷新页面，重绘面板
		panel.validate();// 使重绘的面板确认生效

		// 初始化
		panelInformation.init(panel.getWidth(), panel.getHeight());

		// 更换分割面板
		splitPane.setLeftComponent(panel1);
		splitPane.setRightComponent(panelInformation);
		// 设置分割线大小
		splitPane.setDividerSize(0);
		// 设置分割线位置
		splitPane.setDividerLocation(100);
		// 设置分割线拖动
		splitPane.setEnabled(false);
		// 在分隔符上提供UI小部件以快速扩展/折叠分隔符
		splitPane.setOneTouchExpandable(false);
		return null;
	}
}

/**
 * @Author 作者
 * @Description 说明：搜索访客结果面板
 * @Date 时间：2020-12-3
 */
@SuppressWarnings("serial")
class FindVisitor extends JDialog {
	FindVisitor(Visitor visitor, JFrame parent, boolean modal) {
		super(parent, modal);
		this.setTitle("搜索结果");
		this.setSize(600, 450);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setLayout(new AfAnyWhere());

		JLabel label = new JLabel("搜索结果");
		label.setFont(Constant.FONT1);
		this.add(label, AfMargin.TOP_CENTER);

		this.add(new JLabel("访客ID：" + visitor.getId()), new AfMargin(40, 50, -1, -1));
		this.add(new JLabel("访客昵称：" + visitor.getVisitor_name()), new AfMargin(40, 200, -1, -1));
		this.add(new JLabel("访客账号状态：" + visitor.getVisitor_state()), new AfMargin(40, 350, -1, -1));
		this.add(new JLabel("微博数：" + visitor.getVisitor_weibo_num()), new AfMargin(65, 50, -1, -1));
		this.add(new JLabel("关注数：" + visitor.getVisitor_attention_num()), new AfMargin(65, 200, -1, -1));
		this.add(new JLabel("粉丝数：" + visitor.getVisitor_fans_num()), new AfMargin(65, 350, -1, -1));

		Weibo[] weibos = new Select().select_Writer_id(visitor.getId());
		String[] columnNames = { "微博ID", "微博内容", "点赞数", "微博状态" };// 定义表格列
		String[][] tableValues = new String[weibos.length][columnNames.length];// 定义数组，用来存储表格数据
		for (int i = 0; i < weibos.length; i++) {
			tableValues[i][0] = weibos[i].getWeibo_id();
			tableValues[i][1] = weibos[i].getWeibo_content();
			tableValues[i][2] = weibos[i].getWeibo_like_num().toString();
			tableValues[i][3] = weibos[i].getWeibo_state();
		}
		JTable table = new JTable(tableValues, columnNames);
		table.setSelectionForeground(Color.RED);// 字体颜色
		table.setBackground(Color.PINK);
		table.setSelectionBackground(Color.yellow);// 背景色
		table.setRowHeight(30);// 设置行高30像素
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);// 选择模式(单选)
		table.setEnabled(true);// 可编辑
		table.getTableHeader().setReorderingAllowed(false);// 设置列表头不可重新拖动排列
		int[] width = new int[] { 50, 400, 5, 50 };// width是int型一维数组
		table.setColumnModel(getColumn(table, width)); // 设置不等列宽
		table.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(new JTextField()));
		System.out.println("表格共有" + table.getRowCount() + "行" + table.getColumnCount() + "列");
		JScrollPane scrollPane = new JScrollPane(table);
		this.add(scrollPane, new AfMargin(100, 0, -1, 0));
	}

	/**
	 * @Description 说明：设置表格不等宽度
	 */
	public static TableColumnModel getColumn(JTable table, int[] width) {
		TableColumnModel columns = table.getColumnModel();
		for (int i = 0; i < width.length; i++) {
			TableColumn column = columns.getColumn(i);
			column.setPreferredWidth(width[i]);
		}
		return columns;
	}
}

/**
 * @Author 作者
 * @Description 说明：搜索微博结果面板
 * @Date 时间：2020-12-3
 */
@SuppressWarnings("serial")
class FindWeibo extends JDialog {
	FindWeibo(Weibo weibo, JFrame parent, boolean modal) {
		super(parent, modal);
		this.setTitle("搜索结果");
		this.setSize(600, 450);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setLayout(new AfAnyWhere());

		JLabel label = new JLabel("搜索结果");
		label.setFont(Constant.FONT1);
		this.add(label, AfMargin.TOP_CENTER);

		this.add(new JLabel("微博ID：" + weibo.getWeibo_id()), new AfMargin(40, 50, -1, -1));
		this.add(new JLabel("作者账号：" + weibo.getWriter_id()), new AfMargin(65, 50, -1, -1));
		this.add(new JLabel("点赞数量：" + weibo.getWeibo_like_num()), new AfMargin(90, 50, -1, -1));
		this.add(new JLabel("微博状态：" + weibo.getWeibo_state()), new AfMargin(115, 50, -1, -1));
		this.add(new JLabel("微博内容：" + weibo.getWeibo_content()), new AfMargin(140, 50, -1, -1));
	}

	/**
	 * @Description 说明：设置表格不等宽度
	 */
	public static TableColumnModel getColumn(JTable table, int[] width) {
		TableColumnModel columns = table.getColumnModel();
		for (int i = 0; i < width.length; i++) {
			TableColumn column = columns.getColumn(i);
			column.setPreferredWidth(width[i]);
		}
		return columns;
	}
}