package view_operator;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import data.Constant;
import data.GlobalVar;
import tools.AfAnyWhere;
import tools.AfMargin;
import tools.BackgroundPanel;

/**
 * @Author 作者
 * @Description 说明：操作员信息
 * @Date 时间：2020-12-2
 */
@SuppressWarnings("serial")
public class PanelInformation extends BackgroundPanel {
	public PanelInformation() {
		super(new ImageIcon("images\\背景_2.jpg").getImage());
		this.setLayout(new AfAnyWhere());
	}

	/**
	 * @Description 说明：操作员信息面板初始化
	 */
	public void init(int w, int h) {
		JLabel Title = new JLabel("操作员信息");
		Title.setFont(Constant.FONT1);
		this.add(Title, AfMargin.TOP_CENTER);

		JLabel label = new JLabel("操作员ID：" + GlobalVar.login_operator.getId());
		JLabel label1 = new JLabel("访客待审核数：" + GlobalVar.login_operator.getOperator_visitor_wait());
		JLabel label2 = new JLabel("访客已审核数：" + GlobalVar.login_operator.getOperator_visitor_complete());
		JLabel label3 = new JLabel("微博待审核数：" + GlobalVar.login_operator.getOperator_weibo_wait());
		JLabel label4 = new JLabel("微博已审核数：" + GlobalVar.login_operator.getOperator_weibo_complete());
		label.setFont(Constant.FONT2);
		label1.setFont(Constant.FONT2);
		label2.setFont(Constant.FONT2);
		label3.setFont(Constant.FONT2);
		label4.setFont(Constant.FONT2);
		this.add(label, new AfMargin(100, -1, -1, -1));
		this.add(label1, new AfMargin(150, -1, -1, -1));
		this.add(label2, new AfMargin(200, -1, -1, -1));
		this.add(label3, new AfMargin(250, -1, -1, -1));
		this.add(label4, new AfMargin(300, -1, -1, -1));
	}
}
