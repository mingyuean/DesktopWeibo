package data;

import java.awt.Font;

/**
 * @Author 作者
 * @Description 说明：系统常量的定义
 * @Date 时间：2020-11-28
 */
public class Constant {
	// 状态(login_success\operator_yes_no\visitor_yes_no)
	public static final String YES_STRING = "是", NO_STRING = "否";
	// 权限
	public static final String[] POWER = new String[] { "访客", "操作员", "管理员" };
	// 访客状态
	public static final String[] VISITOR_STATE = new String[] { "正常", "被举报", "封号" };
	// 举报状态
	public static final String[] REPORT_STATE = new String[] { "处理中", "处理完成" };
	// 举报
	public static final String[] REPORT_WEIBO_VISITOR = new String[] { "微博", "访客" };
	// 是否点赞
	public static final String[] WEIBO_TRUE_FALSE = new String[] { "未点赞", "已点赞" };
	// 微博状态
	public static final String[] WEIBO_STATE = new String[] { "正常", "被举报", "被操作员删除" };
	// 字体
	public static final Font FONT1 = new Font("楷体", 1, 30);
	public static final Font FONT2 = new Font("宋体", 0, 14);
	// 空格
	public static final int NUM = 115;
	// 举报人数需达到值
	public static final int REPORT_NUM = 3;
}
