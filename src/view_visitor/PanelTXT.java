package view_visitor;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.border.BevelBorder;

import control.Delete;
import control.Insert;
import control.Select;
import control.Updata;
import data.Constant;
import data.GlobalVar;
import model.Operator;
import model.Report;
import model.Weibo;
import tools.AfAnyWhere;
import tools.AfMargin;

/**
 * @Author 作者
 * @Description 说明：查看微博内容
 * @Date 时间：2020-12-10
 */
@SuppressWarnings("serial")
public class PanelTXT extends JPanel {
	private JTextArea textArea = new JTextArea();
	private JToolBar toolBar = new JToolBar();
	Weibo weibo;

	/**
	 * @Description 说明：构造方法
	 */
	public PanelTXT(Weibo w) {
		super();
		this.weibo = w;
		if (w.getWriter_id().equals(GlobalVar.login_visitor.getId()) == true) {
			init();// 自己的微博
		} else if (w.getWriter_id().equals(GlobalVar.login_visitor.getId()) == false) {
			initwr();// 其他人的微博
		} else if (weibo.getWeibo_state().equals(Constant.WEIBO_STATE[3]) == true) {
			initReportDelete();// 被操作员删除的微博
		} else if (weibo.getWeibo_state().equals(Constant.WEIBO_STATE[1]) == true) {
			initReport();// 审核的微博
		}
	}

	// 被操作员删除的微博
	private void initReportDelete() {
		this.setLayout(new AfAnyWhere());
		this.setPreferredSize(new Dimension(500, 115));
		this.setBackground(new Color(149, 100, 219));
		// 微博内容显示
		textArea.setRows(4);// 4行
		textArea.setBackground(new Color(255, 249, 201));// 背景色
		textArea.setFont(Constant.FONT1);// 字体
		textArea.setEditable(false);// 不可编辑
		textArea.setLineWrap(true);// 自动换行
		textArea.setText("被操作员删除的微博");// 内容

		JScrollPane scrollPane = new JScrollPane(textArea);
		this.add(scrollPane, new AfMargin(0, 0, -1, 0));
		this.add(toolBar, new AfMargin(-1, 0, 0, 0));
	}

	// 审核的微博
	private void initReport() {
		this.setLayout(new AfAnyWhere());
		this.setPreferredSize(new Dimension(500, 115));
		this.setBackground(new Color(149, 100, 219));
		// 微博内容显示
		textArea.setRows(4);// 4行
		textArea.setBackground(new Color(255, 249, 201));// 背景色
		textArea.setFont(Constant.FONT2);// 字体
		textArea.setEditable(false);// 不可编辑
		textArea.setLineWrap(true);// 自动换行
		textArea.setText(weibo.getWeibo_content());// 内容

		// 工具条
		toolBar.setFloatable(false);
		toolBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
		toolBar.add(new JLabel("     赞：" + weibo.getWeibo_like_num()));
		toolBar.add(new JLabel("     微博id：" + weibo.getWeibo_id()));
		toolBar.add(new JLabel("     作者id：" + weibo.getWriter_id()));

		JScrollPane scrollPane = new JScrollPane(textArea);
		this.add(scrollPane, new AfMargin(0, 0, -1, 0));
		this.add(toolBar, new AfMargin(-1, 0, 0, 0));
	}

	// 自己的微博
	private void init() {
		this.setLayout(new AfAnyWhere());
		this.setPreferredSize(new Dimension(500, 115));
		this.setBackground(new Color(149, 100, 219));
		// 微博内容显示
		textArea.setRows(4);// 4行
		textArea.setBackground(new Color(255, 249, 201));// 背景色
		textArea.setFont(Constant.FONT2);// 字体
		textArea.setEditable(false);// 不可编辑
		textArea.setLineWrap(true);// 自动换行
		textArea.setText(weibo.getWeibo_content());// 内容

		// 删除按钮
		JButton button = new JButton(new ImageIcon("images/2_删除.png"));
		button.setToolTipText("删除");
		button.addActionListener((e) -> {
			// 删除
			new Delete().delete(weibo);
			// 更新操作
			GlobalVar.weibo_num1 = GlobalVar.weibo_num1 - 1;
			GlobalVar.login_visitor.setVisitor_weibo_num(GlobalVar.weibo_num1);
			new Updata().updata(GlobalVar.login_visitor);
			// 刷新
			this.getRootPane().getParent().setVisible(false);
			new Interface_MainFrame();
		});

		// 工具条
		toolBar.setFloatable(false);
		toolBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
		toolBar.add(button);
		toolBar.add(new JLabel("     赞：" + weibo.getWeibo_like_num()));
		toolBar.add(new JLabel("     微博id：" + weibo.getWeibo_id()));
		toolBar.add(new JLabel("     作者id：" + weibo.getWriter_id()));
		toolBar.add(new JLabel("     读者id：" + data.GlobalVar.login_visitor.getId()));

		JScrollPane scrollPane = new JScrollPane(textArea);
		this.add(scrollPane, new AfMargin(0, 0, -1, 0));
		this.add(toolBar, new AfMargin(-1, 0, 0, 0));
	}

	// 其他人的微博
	private void initwr() {
		this.setLayout(new AfAnyWhere());
		this.setPreferredSize(new Dimension(500, 115));
		this.setBackground(new Color(149, 100, 219));
		// 微博内容显示
		textArea.setRows(4);// 4行
		textArea.setBackground(new Color(255, 249, 201));// 背景色
		textArea.setFont(Constant.FONT2);// 字体
		textArea.setEditable(false);// 不可编辑
		textArea.setLineWrap(true);// 自动换行
		textArea.setText(weibo.getWeibo_content());// 内容

		// 按钮
		JButton button1 = new JButton();
		button1.setToolTipText("点赞");
		
		if (weibo.getWeibo_true_false().equals(Constant.WEIBO_TRUE_FALSE[0]) == true) {
			// 未点赞
			button1.setIcon(new ImageIcon("images/2_点赞_no.png"));
			button1.addActionListener((e) -> {
				Weibo[] weibos = new Select().select_Reader_id(GlobalVar.login_visitor.getId());
				for (Weibo weibo : weibos) {
					weibo.setWeibo_like_num(weibo.getWeibo_like_num() + 1);
				}
				weibo.setWeibo_true_false(Constant.WEIBO_TRUE_FALSE[1]);
				new Updata().updata(weibo);
			});
		} else if (weibo.getWeibo_true_false().equals(Constant.WEIBO_TRUE_FALSE[1]) == true) {
			// 已点赞
			button1.setIcon(new ImageIcon("images/2_点赞_yes.png"));
			button1.addActionListener((e) -> {
				Weibo[] weibos = new Select().select_Reader_id(GlobalVar.login_visitor.getId());
				for (Weibo weibo : weibos) {
					weibo.setWeibo_like_num(weibo.getWeibo_like_num() + 1);
				}
				weibo.setWeibo_true_false(Constant.WEIBO_TRUE_FALSE[0]);
				new Updata().updata(weibo);
			});
		}

		JButton button2 = new JButton();
		button2.setToolTipText("举报");
		if (weibo.getWeibo_state().equals(Constant.WEIBO_STATE[0]) == true) {
			// 未举报
			button2.setIcon(new ImageIcon("images/2_举报_no.png"));
			button2.addActionListener((e) -> {
				Report report = new Report();
				report.setReport_id(weibo.getWeibo_id());
				new Select().select(report);
				if (report.getReport_num() == 0) {
					report.setReport_num(report.getReport_num() + 1);
					report.setReport_weibo_visitor(Constant.REPORT_WEIBO_VISITOR[0]);
					new Insert().insert(report);
					weibo.setWeibo_state(Constant.WEIBO_STATE[1]);
					new Updata().updata(weibo);
				} else if (report.getReport_num() != 0) {
					if (report.getReport_num() + 1 >= Constant.REPORT_NUM && report.getReport_state().equals(Constant.REPORT_STATE[0])) {
						Operator operator = new Operator();
						new Select().select_RandomNumber(operator);
						operator.setOperator_weibo_wait(operator.getOperator_weibo_wait() + 1);
						report.setReport_num(report.getReport_num() + 1);
						report.setOperator_id(operator.getId());
						new Updata().updata(report);
						weibo.setWeibo_state(Constant.WEIBO_STATE[1]);
						new Updata().updata(weibo);
					} else if (report.getReport_num() + 1 < Constant.REPORT_NUM && report.getReport_state().equals(Constant.REPORT_STATE[0])) {
						report.setReport_num(report.getReport_num() + 1);
						new Updata().updata(report);
						weibo.setWeibo_state(Constant.WEIBO_STATE[1]);
						new Updata().updata(weibo);
					}
				}
			});
		}
		if (weibo.getWeibo_state().equals(Constant.WEIBO_STATE[1]) == true) {
			// 已举报
			button2.setIcon(new ImageIcon("images/2_举报_yes.png"));
			button2.addActionListener((e) -> {
				Report report = new Report();
				report.setReport_id(weibo.getWeibo_id());
				new Select().select(report);
				if (report.getReport_num() - 1 < Constant.REPORT_NUM && report.getReport_state().equals(Constant.REPORT_STATE[0])) {
					Operator operator = new Operator(report.getOperator_id());
					new Select().select(operator);
					operator.setOperator_weibo_wait(operator.getOperator_weibo_wait() - 1);
					report.setReport_num(report.getReport_num() - 1);
					report.setOperator_id("");
					new Updata().updata(report);
					weibo.setWeibo_state(Constant.WEIBO_STATE[0]);
					new Updata().updata(weibo);
				} else if (report.getReport_num() - 1 >= Constant.REPORT_NUM && report.getReport_state().equals(Constant.REPORT_STATE[0])) {
					report.setReport_num(report.getReport_num() - 1);
					new Updata().updata(report);
					weibo.setWeibo_state(Constant.WEIBO_STATE[0]);
					new Updata().updata(weibo);
				}
			});
		}

		// 工具条
		toolBar.setFloatable(false);
		toolBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
		toolBar.add(button1);
		toolBar.add(button2);
		toolBar.add(new JLabel("     赞：" + weibo.getWeibo_like_num()));
		toolBar.add(new JLabel("     微博id：" + weibo.getWeibo_id()));
		toolBar.add(new JLabel("     作者id：" + weibo.getWriter_id()));
		toolBar.add(new JLabel("     读者id：" + data.GlobalVar.login_visitor.getId()));

		JScrollPane scrollPane = new JScrollPane(textArea);
		this.add(scrollPane, new AfMargin(0, 0, -1, 0));
		this.add(toolBar, new AfMargin(-1, 0, 0, 0));
	}
}
