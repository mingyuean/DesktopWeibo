package view_visitor;

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import control.Insert;
import control.Select;
import control.Updata;
import data.Constant;
import data.GlobalVar;
import model.AttentionFans;
import model.Operator;
import model.Report;
import model.Visitor;
import model.Weibo;
import tools.AfAnyWhere;
import tools.AfMargin;
import tools.BackgroundPanel;

/**
 * @Author 作者
 * @Description 说明：查看
 * @Date 时间：2020-12-15
 */
@SuppressWarnings("serial")
public class ShowFram extends JFrame {

	private JButton button1 = new JButton("关注");
	private JButton button2 = new JButton("举报");
	private BackgroundPanel panel1, panel2;
	private JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true);
	private Visitor visitor;

	public ShowFram(Visitor visitor) {
		this.visitor = visitor;
		// 设置窗口标题
		this.setTitle("查看");
		// 设置窗口大小
		this.setSize(600, 450);
		// 居中显示
		this.setLocationRelativeTo(null);
		// 设置图标
		this.setIconImage(new ImageIcon("images\\图标_微博.png").getImage());
		// 设置组件
		init();
		// 将分割面板添加到窗口
		this.add(splitPane);
		// 设置窗口是否可缩放
		this.setResizable(false);
		// 设置窗口是否可见
		this.setVisible(true);
	}

	private void init() {
		panel1 = new BackgroundPanel(new ImageIcon("images\\背景_3.jpg").getImage());
		panel2 = new BackgroundPanel(new ImageIcon("images\\背景_3.jpg").getImage());

		// panel1设置
		panel1.setLayout(new AfAnyWhere());
		JLabel label = new JLabel(visitor.getVisitor_name());
		label.setFont(Constant.FONT1);
		JLabel label1 = new JLabel("微博：" + visitor.getVisitor_weibo_num());
		label1.setFont(Constant.FONT2);
		JLabel label2 = new JLabel("关注：" + visitor.getVisitor_attention_num());
		label2.setFont(Constant.FONT2);
		JLabel label3 = new JLabel("粉丝：" + visitor.getVisitor_fans_num());
		label3.setFont(Constant.FONT2);
		panel1.add(label, new AfMargin(10, 30, -1, -1));
		panel1.add(label1, new AfMargin(-1, 50, 20, -1));
		panel1.add(label2, new AfMargin(-1, 150, 20, -1));
		panel1.add(label3, new AfMargin(-1, 250, 20, -1));
		button1.setFont(Constant.FONT2);
		panel1.add(button1, new AfMargin(-1, -1, 20, 100));
		button2.setFont(Constant.FONT2);
		panel1.add(button2, new AfMargin(-1, -1, 20, 50));

		// panel2设置
		panel2.setLayout(new AfAnyWhere());
		BackgroundPanel p1 = new BackgroundPanel(new ImageIcon("images\\背景_3.jpg").getImage());
		p1.setLayout(new AfAnyWhere());
		p1.setPreferredSize(new Dimension(300, Constant.NUM * visitor.getVisitor_weibo_num()));
		Weibo[] weibos = new Select().select_Writer_id(visitor.getId());
		for (int i = 0; i < weibos.length; i++) {
			p1.add(new PanelTXT(weibos[i]), new AfMargin(Constant.NUM * i, 0, -1, 0));
		}
		JScrollPane scrollPane = new JScrollPane(p1);
		panel2.add(scrollPane);

		// 按钮事件处理
		/* 关注 */
		button1.addActionListener(Event -> {
			AttentionFans attentionFans = new AttentionFans();
			attentionFans.setAttention_id(GlobalVar.login_visitor.getId());
			attentionFans.setFans_id(visitor.getId());
			new Select().select(attentionFans);
			if (new Insert().insert(attentionFans)) {
				// 更新操作(关注数)
				GlobalVar.attention_num = GlobalVar.attention_num + 1;
				GlobalVar.login_visitor.setVisitor_attention_num(GlobalVar.attention_num);
				new Updata().updata(GlobalVar.login_visitor);
				// 更新操作(粉丝数)
				visitor.setVisitor_fans_num(visitor.getVisitor_fans_num() + 1);
				new Updata().updata(visitor);
			}
		});
		/* 举报 */
		if (visitor.getVisitor_state().equals(Constant.VISITOR_STATE[0]) == true) {
			// 未举报
			button2.setToolTipText("未举报");
			button2.addActionListener((e) -> {
				Report report = new Report();
				report.setReport_id(visitor.getId());
				new Select().select(report);
				if (report.getReport_num() == 0) {
					report.setReport_num(report.getReport_num() + 1);
					report.setReport_weibo_visitor(Constant.REPORT_WEIBO_VISITOR[1]);
					new Insert().insert(report);
					visitor.setVisitor_state(Constant.VISITOR_STATE[1]);
					new Updata().updata(visitor);
				} else if (report.getReport_num() != 0) {
					if (report.getReport_num() + 1 >= Constant.REPORT_NUM && report.getReport_state().equals(Constant.REPORT_STATE[0])) {
						Operator operator = new Operator();
						new Select().select_RandomNumber(operator);
						operator.setOperator_weibo_wait(operator.getOperator_weibo_wait() + 1);
						report.setReport_num(report.getReport_num() + 1);
						report.setOperator_id(operator.getId());
						new Updata().updata(report);
						visitor.setVisitor_state(Constant.VISITOR_STATE[1]);
						new Updata().updata(visitor);
					} else if (report.getReport_num() + 1 < Constant.REPORT_NUM && report.getReport_state().equals(Constant.REPORT_STATE[0])) {
						report.setReport_num(report.getReport_num() + 1);
						new Updata().updata(report);
						visitor.setVisitor_state(Constant.VISITOR_STATE[1]);
						new Updata().updata(visitor);
					}
				}
			});
		}
		if (visitor.getVisitor_state().equals(Constant.VISITOR_STATE[1]) == true) {
			// 已举报
			button2.setToolTipText("已举报");
			button2.addActionListener((e) -> {
				Report report = new Report();
				report.setReport_id(visitor.getId());
				new Select().select(report);
				if (report.getReport_num() - 1 < Constant.REPORT_NUM && report.getReport_state().equals(Constant.REPORT_STATE[0])) {
					Operator operator = new Operator(report.getOperator_id());
					new Select().select(operator);
					operator.setOperator_weibo_wait(operator.getOperator_weibo_wait() - 1);
					report.setReport_num(report.getReport_num() - 1);
					report.setOperator_id("");
					new Updata().updata(report);
					visitor.setVisitor_state(Constant.VISITOR_STATE[0]);
					new Updata().updata(visitor);
				} else if (report.getReport_num() - 1 >= Constant.REPORT_NUM && report.getReport_state().equals(Constant.REPORT_STATE[0])) {
					report.setReport_num(report.getReport_num() - 1);
					new Updata().updata(report);
					visitor.setVisitor_state(Constant.VISITOR_STATE[0]);
					new Updata().updata(visitor);
				}
			});
		}

		splitPane.setLeftComponent(panel1);
		splitPane.setRightComponent(panel2);
		// 设置分割线大小
		splitPane.setDividerSize(0);
		// 设置分割线位置
		splitPane.setDividerLocation(100);
		// 设置分割线拖动
		splitPane.setEnabled(false);
		// 在分隔符上提供UI小部件以快速扩展/折叠分隔符
		splitPane.setOneTouchExpandable(false);
	}
}