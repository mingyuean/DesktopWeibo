package tools;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

/**
 * 图片（按比例缩小）
 * 
 * @author 谢晓艳
 */
public class paintImage extends Canvas {

	private static final long serialVersionUID = 1L;

//	方法一: 通过 java.awt.Toolkit 工具类来读取本地、网络 或 内存中 的 图片（支持 GIF、JPEG 或 PNG）
//	Image image = Toolkit.getDefaultToolkit().getImage(String filename);
//	Image image = Toolkit.getDefaultToolkit().getImage(URL url);
//	Image image = Toolkit.getDefaultToolkit().createImage(byte[] imageData);
//
//	方法二: 通过 javax.imageio.ImageIO 工具类读取本地、网络 或 内存中 的 图片（BufferedImage 继承自 Image）
//	BufferedImage bufImage = ImageIO.read(File input);
//	BufferedImage bufImage = ImageIO.read(URL input);
//	BufferedImage bufImage = ImageIO.read(InputStream input);

	private Toolkit toolkit;
	private Image image;
	private int w, h;
	private String filename;

	public paintImage(String filename, int w, int h) {
		this.w = w;
		this.h = h;
		this.filename = filename;
		toolkit = getToolkit();
		image = toolkit.getImage(filename);
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.yellow);
		g2d.drawImage(image, 0, 0, w, h, this);
		g2d.drawOval(0, 0, w, h);
	}

	/**
	 * @return filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param filename 要设置的 filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

}
