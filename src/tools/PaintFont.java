package tools;

import java.awt.Canvas;
import java.awt.Graphics;

public class PaintFont extends Canvas {

	private static final long serialVersionUID = 1L;

	private String strfile;

	public PaintFont(String strfile) {
		this.strfile = strfile;
	}

	@Override
	public void paint(Graphics g) {
		// TODO 自动生成的方法存根
		super.paint(g);
		g.drawString(strfile, 0, 0);
	}
}