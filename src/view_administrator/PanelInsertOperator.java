package view_administrator;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import control.Insert;
import data.Constant;
import model.Operator;
import model.Personal;
import tools.AfAnyWhere;
import tools.AfMargin;
import tools.BackgroundPanel;
import tools.StringUtil;

/**
 * @Author 作者
 * @Description 说明：增加操作员面板
 * @Date 时间：2020-12-2
 */
@SuppressWarnings("serial")
public class PanelInsertOperator extends BackgroundPanel {
	// 按钮
	private JButton insertButton = new JButton("确定增加"), clearButton = new JButton("清空数据");
	// 文本框（输入账号）
	private JTextField idTextField = new JTextField();
	// 密码框（输入密码）
	private JPasswordField password1 = new JPasswordField();
	// 密码框（确认密码）
	private JPasswordField password2 = new JPasswordField();

	public PanelInsertOperator() {
		super(new ImageIcon("images\\背景_2.jpg").getImage());
		this.setLayout(new AfAnyWhere());
	}

	/**
	 * @Description 说明：增加操作员面板初始化
	 */
	public void init(int w, int h) {
		JLabel Title = new JLabel("增加操作员");
		Title.setFont(Constant.FONT1);
		this.add(Title, AfMargin.TOP_CENTER);

		JLabel label1 = new JLabel("输入操作员的账号：");
		label1.setFont(Constant.FONT2);
		JLabel label2 = new JLabel("输入操作员的密码：");
		label2.setFont(Constant.FONT2);
		JLabel label3 = new JLabel("确认输入操作员的密码：");
		label3.setFont(Constant.FONT2);
		this.add(label1, new AfMargin(105, 115, -1, -1));
		this.add(label2, new AfMargin(155, 115, -1, -1));
		this.add(label3, new AfMargin(205, 115, -1, -1));

		idTextField = new JTextField(15);
		idTextField.setToolTipText("账号");
		idTextField.setFont(Constant.FONT2);
		password1 = new JPasswordField(15);
		password1.setToolTipText("密码");
		password1.setFont(Constant.FONT2);
		password2 = new JPasswordField(15);
		password2.setToolTipText("确认密码");
		password2.setFont(Constant.FONT2);
		this.add(idTextField, new AfMargin(100, -1, -1, 115));
		this.add(password1, new AfMargin(150, -1, -1, 115));
		this.add(password2, new AfMargin(200, -1, -1, 115));

		insertButton.setFont(Constant.FONT2);
		clearButton.setFont(Constant.FONT2);
		this.add(insertButton, new AfMargin(-1, 120, 150, -1));
		this.add(clearButton, new AfMargin(-1, -1, 150, 120));
	}

	/**
	 * @Description 说明：增加操作员面板事件处理
	 */
	public void addListener() {
		// 添加
		insertButton.addActionListener((e) -> {
			String id = this.idTextField.getText();
			String password1 = new String(this.password1.getPassword());
			String password2 = new String(this.password2.getPassword());

			if (StringUtil.isEmpty(id)) {
				JOptionPane.showMessageDialog(null, "操作员ID不能为空");
				return;
			}
			if (StringUtil.isEmpty(password1)) {
				JOptionPane.showMessageDialog(null, "密码不能为空");
				return;
			}
			if (StringUtil.isEmpty(password2)) {
				JOptionPane.showMessageDialog(null, "密码不能为空");
				return;
			}
			if (password1.equals(password2) == false) {
				JOptionPane.showMessageDialog(null, "两次密码输入不一致");
				return;
			}
			// 添加操作
			Personal personal = new Personal(id, password1);
			personal.setPower(Constant.POWER[1]);
			Operator operator = new Operator(id);
			new Insert().insert(personal);
			new Insert().insert(operator);
			new Interface_MainFrame();
		});
		// 清空
		clearButton.addActionListener((e) -> {
			idTextField.setText("");
			password1.setText("");
			password2.setText("");
		});
	}
}
