package view_administrator;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import control.Delete;
import control.Select;
import data.Constant;
import model.Operator;
import model.Personal;
import tools.AfAnyWhere;
import tools.AfMargin;
import tools.BackgroundPanel;
import tools.StringUtil;

/**
 * @Author 作者
 * @Description 说明：删除操作员面板
 * @Date 时间：2020-12-2
 */
@SuppressWarnings("serial")
public class PanelDeleteOperator extends BackgroundPanel {
	// 按钮
	private JButton deleteButton = new JButton("确定删除"), showButton = new JButton("查看信息");
	// 文本框（输入账号）
	private JTextField idTextField = new JTextField();

	public PanelDeleteOperator() {
		super(new ImageIcon("images\\背景_2.jpg").getImage());
		this.setLayout(new AfAnyWhere());
	}

	/**
	 * @Description 说明：删除操作员面板初始化
	 */
	public void init(int w, int h) {
		JLabel Title = new JLabel("删除操作员");
		Title.setFont(Constant.FONT1);
		this.add(Title, AfMargin.TOP_CENTER);

		JLabel label = new JLabel("输入需要删除的操作员的账号：");
		label.setFont(Constant.FONT2);
		this.add(label, new AfMargin(60, -1, -1, -1));

		idTextField = new JTextField(15);
		idTextField.setToolTipText("账号");
		idTextField.setFont(Constant.FONT2);
		this.add(idTextField, new AfMargin(85, -1, -1, -1));

		deleteButton.setFont(Constant.FONT2);
		showButton.setFont(Constant.FONT2);
		this.add(deleteButton, new AfMargin(-1, 120, 150, -1));
		this.add(showButton, new AfMargin(-1, -1, 150, 120));
	}

	/**
	 * @Description 说明：删除操作员面板事件处理
	 */
	public void addListener() {
		// 删除
		deleteButton.addActionListener((e) -> {
			String id = this.idTextField.getText();
			if (StringUtil.isEmpty(id)) {
				JOptionPane.showMessageDialog(null, "操作员ID不能为空");
				return;
			}
			/* 个人 */
			Personal personal = new Personal(id);
			new Select().select(personal);
			/* 操作员 */
			Operator operator = new Operator(id);
			new Select().select(operator);
			if (operator.getOperator_yes_no().equals(Constant.YES_STRING) == true) {
				new Delete().delete(personal);
				new Delete().delete(operator);
				new Interface_MainFrame();
			} else {
				System.out.println("不可查看");
				JOptionPane.showMessageDialog(null, "账号不存在", "警告", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		// 查看
		showButton.addActionListener((e) -> {
			String id = this.idTextField.getText();
			if (StringUtil.isEmpty(id)) {
				JOptionPane.showMessageDialog(null, "操作员ID不能为空");
				return;
			}
			/* 操作员 */
			Operator operator = new Operator(id);
			new Select().select(operator);
			if (operator.getOperator_yes_no().equals(Constant.YES_STRING) == true) {
				FindOperator dialog = new FindOperator(operator, (JFrame) this.getRootPane().getParent(), true);
				dialog.setVisible(true);
			} else {
				System.out.println("不可查看");
				JOptionPane.showMessageDialog(null, "账号不存在", "警告", JOptionPane.INFORMATION_MESSAGE);
			}
		});
	}
}

/**
 * @Author 作者
 * @Description 说明：搜索操作员结果面板
 * @Date 时间：2020-12-3
 */
@SuppressWarnings("serial")
class FindOperator extends JDialog {
	FindOperator(Operator operator, JFrame parent, boolean modal) {
		super(parent, modal);
		this.setTitle("搜索结果");
		this.setSize(200, 350);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setLayout(new AfAnyWhere());

		JLabel label = new JLabel("删除操作员的信息");
		label.setFont(Constant.FONT2);
		this.add(label, new AfMargin(10, -1, -1, -1));
		this.add(new JLabel("操作员账号：" + operator.getId()), new AfMargin(50, -1, -1, -1));
		this.add(new JLabel("访客待审核数：" + operator.getOperator_visitor_wait()), new AfMargin(80, -1, -1, -1));
		this.add(new JLabel("访客已审核数：" + operator.getOperator_visitor_complete()), new AfMargin(110, -1, -1, -1));
		this.add(new JLabel("微博待审核数：" + operator.getOperator_weibo_wait()), new AfMargin(140, -1, -1, -1));
		this.add(new JLabel("微博已审核数：" + operator.getOperator_weibo_complete()), new AfMargin(170, -1, -1, -1));
	}
}