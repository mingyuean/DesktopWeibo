package view_administrator;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import control.ResultSetNum;
import control.Select;
import data.Constant;
import model.Operator;
import tools.AfAnyWhere;
import tools.AfMargin;
import tools.BackgroundPanel;

/**
 * @Author 作者
 * @Description 说明：查看操作员面板
 * @Date 时间：2020-12-2
 */
@SuppressWarnings("serial")
public class PanelSelectOperator extends BackgroundPanel {

	public PanelSelectOperator() {
		super(new ImageIcon("images\\背景_2.jpg").getImage());
		this.setLayout(new AfAnyWhere());
	}

	/**
	 * @Description 说明：查看操作员面板初始化
	 */
	public void init(int w, int h) {
		JLabel Title = new JLabel("查看操作员");
		Title.setFont(Constant.FONT1);
		this.add(Title, AfMargin.TOP_CENTER);
		JLabel numJLabel = new JLabel("操作员总数：" + new ResultSetNum().resultSetNumOperator());
		numJLabel.setFont(Constant.FONT2);
		this.add(numJLabel, new AfMargin(45, -1, -1, -1));

		Operator[] visitors = new Select().select_Operator();
		String[] columnNames = { "ID", "访客待审核数", "访客已审核数", "微博待审核数", "微博已审核数" };// 定义表格列
		String[][] tableValues = new String[visitors.length][columnNames.length];// 定义数组，用来存储表格数据
		for (int i = 0; i < visitors.length; i++) {
			tableValues[i][0] = visitors[i].getId();
			tableValues[i][1] = visitors[i].getOperator_visitor_wait().toString();
			tableValues[i][2] = visitors[i].getOperator_visitor_complete().toString();
			tableValues[i][3] = visitors[i].getOperator_weibo_wait().toString();
			tableValues[i][4] = visitors[i].getOperator_weibo_complete().toString();
		}
		JTable table = new JTable(tableValues, columnNames);
		table.setSelectionForeground(Color.RED);// 字体颜色
		table.setBackground(Color.PINK);
		table.setSelectionBackground(Color.yellow);// 背景色
		table.setRowHeight(30);// 设置行高30像素
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);// 选择模式(单选)
		table.setEnabled(false);// 不可编辑
		table.getTableHeader().setReorderingAllowed(false);// 设置列表头不可重新拖动排列
		System.out.println("表格共有" + table.getRowCount() + "行" + table.getColumnCount() + "列");
		JScrollPane scrollPane = new JScrollPane(table);
		this.add(scrollPane, new AfMargin(60, -1, 50, -1));
	}
}
