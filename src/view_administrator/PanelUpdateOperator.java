package view_administrator;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import control.Delete;
import control.Insert;
import control.Select;
import data.Constant;
import model.Operator;
import model.Personal;
import tools.AfAnyWhere;
import tools.AfMargin;
import tools.BackgroundPanel;
import tools.StringUtil;

/**
 * @Author 作者
 * @Description 说明：修改操作员面板
 * @Date 时间：2020-12-2
 */
@SuppressWarnings("serial")
public class PanelUpdateOperator extends BackgroundPanel {
	// 按钮
	private JButton updataButton = new JButton("编辑信息"), showButton = new JButton("查看信息");
	// 文本框（输入账号）
	private JTextField idTextField = new JTextField();

	public PanelUpdateOperator() {
		super(new ImageIcon("images\\背景_2.jpg").getImage());
		this.setLayout(new AfAnyWhere());
	}

	/**
	 * @Description 说明：修改操作员面板初始化
	 */
	public void init(int w, int h) {
		JLabel Title = new JLabel("修改操作员");
		Title.setFont(Constant.FONT1);
		this.add(Title, AfMargin.TOP_CENTER);

		JLabel label = new JLabel("输入需要修改的操作员的账号：");
		label.setFont(Constant.FONT2);
		this.add(label, new AfMargin(60, -1, -1, -1));

		idTextField = new JTextField(15);
		idTextField.setToolTipText("账号");
		idTextField.setFont(Constant.FONT2);
		this.add(idTextField, new AfMargin(85, -1, -1, -1));

		updataButton.setFont(Constant.FONT2);
		showButton.setFont(Constant.FONT2);
		this.add(updataButton, new AfMargin(-1, 120, 150, -1));
		this.add(showButton, new AfMargin(-1, -1, 150, 120));
	}

	/**
	 * @Description 说明：修改操作员面板事件处理
	 */
	public void addListener() {
		// 修改
		updataButton.addActionListener((e) -> {
			String id = this.idTextField.getText();
			if (StringUtil.isEmpty(id)) {
				JOptionPane.showMessageDialog(null, "操作员ID不能为空");
				return;
			}
			/* 个人 */
			Personal personal = new Personal(id);
			new Select().select(personal);
			/* 操作员 */
			Operator operator = new Operator(id);
			new Select().select(operator);
			if (operator.getOperator_yes_no().equals(Constant.YES_STRING) == true) {
				UpdataOperator dialog = new UpdataOperator(personal, operator, (JFrame) this.getRootPane().getParent(), true);
				dialog.setVisible(true);
			} else {
				System.out.println("不可查看");
				JOptionPane.showMessageDialog(null, "账号不存在", "警告", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		// 查看
		showButton.addActionListener((e) -> {
			String id = this.idTextField.getText();
			if (StringUtil.isEmpty(id)) {
				JOptionPane.showMessageDialog(null, "操作员ID不能为空");
				return;
			}
			/* 操作员 */
			Operator operator = new Operator(id);
			new Select().select(operator);
			if (operator.getOperator_yes_no().equals(Constant.YES_STRING) == true) {
				FindOperator dialog = new FindOperator(operator, (JFrame) this.getRootPane().getParent(), true);
				dialog.setVisible(true);
			} else {
				System.out.println("不可查看");
				JOptionPane.showMessageDialog(null, "账号不存在", "警告", JOptionPane.INFORMATION_MESSAGE);
			}
		});
	}
}

/**
 * @Author 作者
 * @Description 说明：搜索操作员结果面板
 * @Date 时间：2020-12-3
 */
@SuppressWarnings("serial")
class UpdataOperator extends JDialog {
	UpdataOperator(Personal personal, Operator operator, JFrame parent, boolean modal) {
		super(parent, modal);
		this.setTitle("搜索结果");
		this.setSize(200, 350);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setLayout(new AfAnyWhere());

		JLabel label = new JLabel("修改操作员的信息");
		label.setFont(Constant.FONT2);
		this.add(label, new AfMargin(10, -1, -1, -1));
		this.add(new JLabel("操作员账号："), new AfMargin(50, 15, -1, -1));
		this.add(new JLabel("操作员密码："), new AfMargin(80, 15, -1, -1));
		this.add(new JLabel("访客待审核数："), new AfMargin(110, 15, -1, -1));
		this.add(new JLabel("访客已审核数："), new AfMargin(140, 15, -1, -1));
		this.add(new JLabel("微博待审核数："), new AfMargin(170, 15, -1, -1));
		this.add(new JLabel("微博已审核数："), new AfMargin(200, 15, -1, -1));

		JTextField TextField1 = new JTextField(personal.getId(), 6);
		JTextField TextField2 = new JTextField(personal.getPassword(), 6);
		JTextField TextField3 = new JTextField(operator.getOperator_visitor_wait().toString(), 6);
		JTextField TextField4 = new JTextField(operator.getOperator_visitor_complete().toString(), 6);
		JTextField TextField5 = new JTextField(operator.getOperator_weibo_wait().toString(), 6);
		JTextField TextField6 = new JTextField(operator.getOperator_weibo_complete().toString(), 6);
		this.add(TextField1, new AfMargin(45, -1, -1, 15));
		this.add(TextField2, new AfMargin(75, -1, -1, 15));
		this.add(TextField3, new AfMargin(105, -1, -1, 15));
		this.add(TextField4, new AfMargin(135, -1, -1, 15));
		this.add(TextField5, new AfMargin(165, -1, -1, 15));
		this.add(TextField6, new AfMargin(195, -1, -1, 15));

		JButton button1 = new JButton("确定编辑");
		JButton button2 = new JButton("清空数据");
		button1.setFont(Constant.FONT2);
		button2.setFont(Constant.FONT2);
		this.add(button1, new AfMargin(-1, 10, 50, -1));
		this.add(button2, new AfMargin(-1, -1, 50, 10));
		// 修改
		button1.addActionListener((e) -> {
			if (StringUtil.isEmpty(TextField1.getText()) || StringUtil.isEmpty(TextField2.getText()) || StringUtil.isEmpty(TextField3.getText())
					|| StringUtil.isEmpty(TextField4.getText()) || StringUtil.isEmpty(TextField5.getText())
					|| StringUtil.isEmpty(TextField6.getText())) {
				JOptionPane.showMessageDialog(null, "输入框不能为空");
				return;
			}
			new Delete().delete(personal);
			new Delete().delete(operator);

			personal.setId(TextField1.getText());
			personal.setPassword(TextField2.getText());
			personal.setPower(Constant.POWER[1]);
			new Insert().insert(personal);

			operator.setId(TextField1.getText());
			operator.setOperator_visitor_wait(Integer.parseInt(TextField3.getText()));
			operator.setOperator_visitor_complete(Integer.parseInt(TextField4.getText()));
			operator.setOperator_weibo_complete(Integer.parseInt(TextField5.getText()));
			operator.setOperator_weibo_complete(Integer.parseInt(TextField6.getText()));
			new Insert().insert(operator);

			this.setVisible(false);
			new Interface_MainFrame();
		});
		// 清空
		button2.addActionListener((e) -> {
			TextField1.setText("");
			TextField2.setText("");
			TextField3.setText("");
			TextField4.setText("");
			TextField5.setText("");
			TextField6.setText("");
		});
	}
}