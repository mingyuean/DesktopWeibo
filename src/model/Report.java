package model;

import data.Constant;

public class Report {
	/** 成员变量 **/
	// 操作员账号
	private String operator_id;
	// 微博编号或访客账号
	private String report_id;
	// 举报状态
	private String report_state;
	// 举报
	private String report_weibo_visitor;
	// 举报人数
	private Integer report_num;
	// 举报处理意见
	private String report_content;

	public Report() {
		this.report_num = 0;
		this.report_state = Constant.REPORT_STATE[0];
	}

	public Report(String report_id) {
		this.report_state = report_id;
		this.report_num = 0;
		this.report_state = Constant.REPORT_STATE[0];
	}

	/**
	 * @Description 说明：获取
	 * @return operator_id
	 */
	public String getOperator_id() {
		return operator_id;
	}

	/**
	 * @Description 说明：设置
	 * @param operator_id 要设置的 operator_id
	 */
	public void setOperator_id(String operator_id) {
		this.operator_id = operator_id;
	}

	/**
	 * @Description 说明：获取
	 * @return report_id
	 */
	public String getReport_id() {
		return report_id;
	}

	/**
	 * @Description 说明：设置
	 * @param report_id 要设置的 report_id
	 */
	public void setReport_id(String report_id) {
		this.report_id = report_id;
	}

	/**
	 * @Description 说明：获取
	 * @return report_state
	 */
	public String getReport_state() {
		return report_state;
	}

	/**
	 * @Description 说明：设置
	 * @param report_state 要设置的 report_state
	 */
	public void setReport_state(String report_state) {
		this.report_state = report_state;
	}

	/**
	 * @Description 说明：获取
	 * @return report_weibo_visitor
	 */
	public String getReport_weibo_visitor() {
		return report_weibo_visitor;
	}

	/**
	 * @Description 说明：设置
	 * @param report_weibo_visitor 要设置的 report_weibo_visitor
	 */
	public void setReport_weibo_visitor(String report_weibo_visitor) {
		this.report_weibo_visitor = report_weibo_visitor;
	}

	/**
	 * @Description 说明：获取
	 * @return report_num
	 */
	public Integer getReport_num() {
		return report_num;
	}

	/**
	 * @Description 说明：设置
	 * @param report_num 要设置的 report_num
	 */
	public void setReport_num(Integer report_num) {
		this.report_num = report_num;
	}

	/**
	 * @Description 说明：获取
	 * @return report_content
	 */
	public String getReport_content() {
		return report_content;
	}

	/**
	 * @Description 说明：设置
	 * @param report_content 要设置的 report_content
	 */
	public void setReport_content(String report_content) {
		this.report_content = report_content;
	}

}
