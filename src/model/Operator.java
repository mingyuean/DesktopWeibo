package model;

import data.Constant;

/**
 * @Author 作者
 * @Description 说明：用于设置、获取操作员对象的属性信息
 * @Date 时间：2020-11-25
 */
public class Operator {
	/** 成员变量 **/
	// 操作员账号
	private String id;
	// 访客待审核数
	private Integer operator_visitor_wait;
	// 访客已审核数
	private Integer operator_visitor_complete;
	// 微博待审核数
	private Integer operator_weibo_wait;
	// 微博已审核数
	private Integer operator_weibo_complete;
	// 是否可查看
	private String operator_yes_no;

	public Operator() {
	}

	/**
	 * @Description 说明
	 */
	public Operator(String id) {
		this.id = id;
		this.operator_visitor_wait = 0;
		this.operator_visitor_complete = 0;
		this.operator_weibo_wait = 0;
		this.operator_weibo_complete = 0;
		this.operator_yes_no = Constant.NO_STRING;
	}

	/**
	 * @Description 说明：获取
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @Description 说明：设置
	 * @param id 要设置的 id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @Description 说明：获取
	 * @return operator_visitor_wait
	 */
	public Integer getOperator_visitor_wait() {
		return operator_visitor_wait;
	}

	/**
	 * @Description 说明：设置
	 * @param operator_visitor_wait 要设置的 operator_visitor_wait
	 */
	public void setOperator_visitor_wait(Integer operator_visitor_wait) {
		this.operator_visitor_wait = operator_visitor_wait;
	}

	/**
	 * @Description 说明：获取
	 * @return operator_visitor_complete
	 */
	public Integer getOperator_visitor_complete() {
		return operator_visitor_complete;
	}

	/**
	 * @Description 说明：设置
	 * @param operator_visitor_complete 要设置的 operator_visitor_complete
	 */
	public void setOperator_visitor_complete(Integer operator_visitor_complete) {
		this.operator_visitor_complete = operator_visitor_complete;
	}

	/**
	 * @Description 说明：获取
	 * @return operator_weibo_wait
	 */
	public Integer getOperator_weibo_wait() {
		return operator_weibo_wait;
	}

	/**
	 * @Description 说明：设置
	 * @param operator_weibo_wait 要设置的 operator_weibo_wait
	 */
	public void setOperator_weibo_wait(Integer operator_weibo_wait) {
		this.operator_weibo_wait = operator_weibo_wait;
	}

	/**
	 * @Description 说明：获取
	 * @return operator_weibo_complete
	 */
	public Integer getOperator_weibo_complete() {
		return operator_weibo_complete;
	}

	/**
	 * @Description 说明：设置
	 * @param operator_weibo_complete 要设置的 operator_weibo_complete
	 */
	public void setOperator_weibo_complete(Integer operator_weibo_complete) {
		this.operator_weibo_complete = operator_weibo_complete;
	}

	/**
	 * @Description 说明：获取
	 * @return operator_yes_no
	 */
	public String getOperator_yes_no() {
		return operator_yes_no;
	}

	/**
	 * @Description 说明：设置
	 * @param operator_yes_no 要设置的 operator_yes_no
	 */
	public void setOperator_yes_no(String operator_yes_no) {
		this.operator_yes_no = operator_yes_no;
	}

}
