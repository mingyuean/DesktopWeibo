package model;

import data.Constant;

/**
 * @Author 作者
 * @Description 说明：用于设置、获取访客对象的属性信息
 * @Date 时间：2020-11-25
 */
public class Visitor {
	/** 成员变量 **/
	// 访客账号
	private String id;
	// 访客昵称
	private String visitor_name;
	// 访客头像
	private String visitor_image;
	// 访客性别
	private String visitor_sex;
	// 访客生日
	private String visitor_birthday;
	// 账号状态
	private String visitor_state;
	// 是否可查看
	private String visitor_yes_no;
	// 用户微博数
	private Integer visitor_weibo_num;
	// 用户关注数
	private Integer visitor_attention_num;
	// 用户粉丝数
	private Integer visitor_fans_num;

	public Visitor() {
	}

	/**
	 * @Description 说明
	 */
	public Visitor(String id) {
		this.id = id;
		this.visitor_name = "用户" + id;
		this.visitor_image = "images\\头像.jpg";
		this.visitor_sex = "请进入编辑界面";
		this.visitor_birthday = "请进入编辑界面";
		this.visitor_state = Constant.VISITOR_STATE[0];
		this.visitor_yes_no = Constant.NO_STRING;
		this.visitor_weibo_num = 0;
		this.visitor_attention_num = 1;
		this.visitor_fans_num = 1;
	}

	/**
	 * @Description 说明：获取
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @Description 说明：设置
	 * @param id 要设置的 id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @Description 说明：获取
	 * @return visitor_name
	 */
	public String getVisitor_name() {
		return visitor_name;
	}

	/**
	 * @Description 说明：设置
	 * @param visitor_name 要设置的 visitor_name
	 */
	public void setVisitor_name(String visitor_name) {
		this.visitor_name = visitor_name;
	}

	/**
	 * @Description 说明：获取
	 * @return visitor_image
	 */
	public String getVisitor_image() {
		return visitor_image;
	}

	/**
	 * @Description 说明：设置
	 * @param visitor_image 要设置的 visitor_image
	 */
	public void setVisitor_image(String visitor_image) {
		this.visitor_image = visitor_image;
	}

	/**
	 * @Description 说明：获取
	 * @return visitor_sex
	 */
	public String getVisitor_sex() {
		return visitor_sex;
	}

	/**
	 * @Description 说明：设置
	 * @param visitor_sex 要设置的 visitor_sex
	 */
	public void setVisitor_sex(String visitor_sex) {
		this.visitor_sex = visitor_sex;
	}

	/**
	 * @Description 说明：获取
	 * @return visitor_birthday
	 */
	public String getVisitor_birthday() {
		return visitor_birthday;
	}

	/**
	 * @Description 说明：设置
	 * @param visitor_birthday 要设置的 visitor_birthday
	 */
	public void setVisitor_birthday(String visitor_birthday) {
		this.visitor_birthday = visitor_birthday;
	}

	/**
	 * @Description 说明：获取
	 * @return visitor_state
	 */
	public String getVisitor_state() {
		return visitor_state;
	}

	/**
	 * @Description 说明：设置
	 * @param visitor_state 要设置的 visitor_state
	 */
	public void setVisitor_state(String visitor_state) {
		this.visitor_state = visitor_state;
	}

	/**
	 * @Description 说明：获取
	 * @return visitor_yes_no
	 */
	public String getVisitor_yes_no() {
		return visitor_yes_no;
	}

	/**
	 * @Description 说明：设置
	 * @param visitor_yes_no 要设置的 visitor_yes_no
	 */
	public void setVisitor_yes_no(String visitor_yes_no) {
		this.visitor_yes_no = visitor_yes_no;
	}

	/**
	 * @Description 说明：获取
	 * @return visitor_weibo_num
	 */
	public Integer getVisitor_weibo_num() {
		return visitor_weibo_num;
	}

	/**
	 * @Description 说明：设置
	 * @param visitor_weibo_num 要设置的 visitor_weibo_num
	 */
	public void setVisitor_weibo_num(Integer visitor_weibo_num) {
		this.visitor_weibo_num = visitor_weibo_num;
	}

	/**
	 * @Description 说明：获取
	 * @return visitor_attention_num
	 */
	public Integer getVisitor_attention_num() {
		return visitor_attention_num;
	}

	/**
	 * @Description 说明：设置
	 * @param visitor_attention_num 要设置的 visitor_attention_num
	 */
	public void setVisitor_attention_num(Integer visitor_attention_num) {
		this.visitor_attention_num = visitor_attention_num;
	}

	/**
	 * @Description 说明：获取
	 * @return visitor_fans_num
	 */
	public Integer getVisitor_fans_num() {
		return visitor_fans_num;
	}

	/**
	 * @Description 说明：设置
	 * @param visitor_fans_num 要设置的 visitor_fans_num
	 */
	public void setVisitor_fans_num(Integer visitor_fans_num) {
		this.visitor_fans_num = visitor_fans_num;
	}

}
