package model;

import data.Constant;

/**
 * @Author 作者
 * @Description 说明：用于设置、获取微博对象的属性信息
 * @Date 时间：2020-12-10
 */
public class Weibo {
	/** 成员变量 **/
	// 微博编号
	private String weibo_id;
	// 作者账号
	private String writer_id;
	// 读者账号
	private String resder_id;
	// 微博内容
	private String weibo_content;
	// 点赞数量
	private Integer weibo_like_num;
	// 是否点赞（true/1——已点赞，false/0——未点赞）
	private String weibo_true_false;
	// 微博状态（true/1——正常，false/0——被举报）
	private String weibo_state;

	public Weibo() {
		this.weibo_like_num = 0;
		this.weibo_true_false = Constant.WEIBO_TRUE_FALSE[0];
		this.weibo_state = Constant.WEIBO_STATE[0];
	}

	public Weibo(String weibo_id) {
		this.weibo_id = weibo_id;
		this.weibo_like_num = 0;
		this.weibo_true_false = Constant.WEIBO_TRUE_FALSE[0];
		this.weibo_state = Constant.WEIBO_STATE[0];
	}

	/**
	 * @Description 说明：获取
	 * @return weibo_id
	 */
	public String getWeibo_id() {
		return weibo_id;
	}

	/**
	 * @Description 说明：设置
	 * @param weibo_id 要设置的 weibo_id
	 */
	public void setWeibo_id(String weibo_id) {
		this.weibo_id = weibo_id;
	}

	/**
	 * @Description 说明：获取
	 * @return writer_id
	 */
	public String getWriter_id() {
		return writer_id;
	}

	/**
	 * @Description 说明：设置
	 * @param writer_id 要设置的 writer_id
	 */
	public void setWriter_id(String writer_id) {
		this.writer_id = writer_id;
	}

	/**
	 * @Description 说明：获取
	 * @return resder_id
	 */
	public String getResder_id() {
		return resder_id;
	}

	/**
	 * @Description 说明：设置
	 * @param resder_id 要设置的 resder_id
	 */
	public void setResder_id(String resder_id) {
		this.resder_id = resder_id;
	}

	/**
	 * @Description 说明：获取
	 * @return weibo_content
	 */
	public String getWeibo_content() {
		return weibo_content;
	}

	/**
	 * @Description 说明：设置
	 * @param weibo_content 要设置的 weibo_content
	 */
	public void setWeibo_content(String weibo_content) {
		this.weibo_content = weibo_content;
	}

	/**
	 * @Description 说明：获取
	 * @return weibo_like_num
	 */
	public Integer getWeibo_like_num() {
		return weibo_like_num;
	}

	/**
	 * @Description 说明：设置
	 * @param weibo_like_num 要设置的 weibo_like_num
	 */
	public void setWeibo_like_num(Integer weibo_like_num) {
		this.weibo_like_num = weibo_like_num;
	}

	/**
	 * @Description 说明：获取
	 * @return weibo_true_false
	 */
	public String getWeibo_true_false() {
		return weibo_true_false;
	}

	/**
	 * @Description 说明：设置
	 * @param weibo_true_false 要设置的 weibo_true_false
	 */
	public void setWeibo_true_false(String weibo_true_false) {
		this.weibo_true_false = weibo_true_false;
	}

	/**
	 * @Description 说明：获取
	 * @return weibo_state
	 */
	public String getWeibo_state() {
		return weibo_state;
	}

	/**
	 * @Description 说明：设置
	 * @param weibo_state 要设置的 weibo_state
	 */
	public void setWeibo_state(String weibo_state) {
		this.weibo_state = weibo_state;
	}

}
