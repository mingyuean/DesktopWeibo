/*
Navicat MySQL Data Transfer

Source Server         : Test
Source Server Version : 50732
Source Host           : localhost:3306
Source Database       : desktopweibo

Target Server Type    : MYSQL
Target Server Version : 50732
File Encoding         : 65001

Date: 2020-12-02 18:47:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for operator
-- ----------------------------
DROP TABLE IF EXISTS `operator`;
CREATE TABLE `operator` (
  `id` varchar(50) NOT NULL COMMENT '操作员账号',
  `operator_visitor_wait` int(11) NOT NULL DEFAULT '0' COMMENT '访客待审核数',
  `operator_visitor_complete` int(11) NOT NULL DEFAULT '0' COMMENT '访客已审核数',
  `operator_weibo_wait` int(11) NOT NULL DEFAULT '0' COMMENT '微博待审核数',
  `operator_weibo_complete` int(11) NOT NULL DEFAULT '0' COMMENT '微博已审核数',
  `operator_yes_no` varchar(20) NOT NULL DEFAULT '1' COMMENT '是否可查看（true/1——可查看，false/0——不可查看）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of operator
-- ----------------------------

-- ----------------------------
-- Table structure for personal
-- ----------------------------
DROP TABLE IF EXISTS `personal`;
CREATE TABLE `personal` (
  `id` varchar(50) NOT NULL COMMENT '个人账号',
  `password` varchar(50) NOT NULL COMMENT '个人密码',
  `power` varchar(5) NOT NULL DEFAULT '1' COMMENT '个人操作权限（1——访客，2——操作员，3——管理员）',
  `login_success` varchar(5) NOT NULL DEFAULT '0' COMMENT '是否登录成功（true/1——登录成功，false/0——登录失败）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of personal
-- ----------------------------
INSERT INTO `personal` VALUES ('1', '1', '3', '0');
INSERT INTO `personal` VALUES ('123', '123', '1', '0');
INSERT INTO `personal` VALUES ('2', '2', '2', '0');

-- ----------------------------
-- Table structure for register
-- ----------------------------
DROP TABLE IF EXISTS `register`;
CREATE TABLE `register` (
  `UserID` varchar(255) NOT NULL COMMENT '账号ID',
  `UserPassword` varchar(255) NOT NULL COMMENT '账号密码',
  `Birth` datetime NOT NULL COMMENT '注册日期'
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of register
-- ----------------------------
INSERT INTO `register` VALUES ('123', '123', '2020-11-14 08:21:00');

-- ----------------------------
-- Table structure for visitor
-- ----------------------------
DROP TABLE IF EXISTS `visitor`;
CREATE TABLE `visitor` (
  `id` varchar(50) NOT NULL COMMENT '访客账号',
  `visitor_name` varchar(50) NOT NULL COMMENT '访客昵称',
  `visitor_image` varchar(50) NOT NULL COMMENT '访客头像',
  `visitor_sex` varchar(50) NOT NULL COMMENT '访客性别',
  `visitor_birthday` varchar(50) NOT NULL COMMENT '访客生日',
  `visitor_num_grade` int(11) NOT NULL DEFAULT '0' COMMENT '访客等级',
  `visitor_num_attention` int(11) NOT NULL DEFAULT '0' COMMENT '关注数',
  `visitor_num_fans` int(11) NOT NULL DEFAULT '0' COMMENT '粉丝数',
  `visitor_num_weibo` int(11) NOT NULL DEFAULT '0' COMMENT '微博数',
  `visitor_state` varchar(20) NOT NULL DEFAULT '1' COMMENT '访客账号状态（1——正常、2——禁言、3——封号）',
  `visitor_yes_no` varchar(20) NOT NULL DEFAULT '1' COMMENT '是否可查看（true/1——可查看，false/0——不可查看）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of visitor
-- ----------------------------
INSERT INTO `visitor` VALUES ('12', '用户12', 'images\\头像.jpg', '请进入编辑界面', '请进入编辑界面', '0', '0', '0', '0', '1', '0');
INSERT INTO `visitor` VALUES ('123', '用户123', 'images\\头像.jpg', '请进入编辑界面', '请进入编辑界面', '0', '0', '0', '0', '1', '0');
INSERT INTO `visitor` VALUES ('1234', '用户1234', 'images\\头像.jpg', '请进入编辑界面', '请进入编辑界面', '0', '0', '0', '0', '1', '0');

-- ----------------------------
-- Table structure for weibo
-- ----------------------------
DROP TABLE IF EXISTS `weibo`;
CREATE TABLE `weibo` (
  `weibo_id` varchar(50) NOT NULL,
  `visiter_id` varchar(50) NOT NULL,
  `weibo_content` text,
  `weibo_comments` text,
  `weibo_state` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`weibo_id`,`visiter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of weibo
-- ----------------------------
